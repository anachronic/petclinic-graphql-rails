# README

This is a toy project intended to learn rails 6 with webpack and graphql

You can find the guide [here](https://evilmartians.com/chronicles/graphql-on-rails-1-from-zero-to-the-first-query)

I did, however, set webpack to use Vue instead of react, because I'm more familiar with Vue rather than with React.
