module Types
  class OwnerType < Types::BaseObject
    field(:id, ID, null: false)
    field(:name, String, null: false)
    field(:last_name, String, null: false)
    field(:email, String, null: true)
  end
end
