module Types
  class PetType < Types::BaseObject
    field(:id, ID, null: false)
    field(:name, String, null: false)
    field(:age, Integer, null: false)
    field(:owner, Types::OwnerType, null: true)
  end
end
