module Types
  class QueryType < Types::BaseObject

    field(:pets, [Types::PetType], "Returns a list of all pets", null: false)
    field(:owner, [Types::OwnerType], null: false)

    def pets
      Pet.all
    end
  end
end
