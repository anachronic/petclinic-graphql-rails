import Vue from "vue";
import App from "../app.vue";
import VueApollo from "vue-apollo";
import { apolloClient } from "../conf/apollo.conf";

Vue.use(VueApollo);

const apolloProvider = new VueApollo({ defaultClient: apolloClient });

document.addEventListener("DOMContentLoaded", () => {
  const app = new Vue({
    apolloProvider,
    render: h => h(App)
  }).$mount();
  document.body.appendChild(app.$el);
});
