import ApolloClient from "apollo-boost";

function getToken() {
  return document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");
}

export const apolloClient = new ApolloClient({
  uri: "http://localhost:3000/graphql",
  headers: {
    "X-CSRF-Token": getToken()
  }
});
