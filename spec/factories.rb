FactoryBot.define do
  factory :owner do
    sequence(:name) { |n| "User #{n}" }
    sequence(:last_name) { |n| n }
    sequence(:email) { "#{name}@example.com".downcase }
  end

  factory :pet do
    sequence(:name) { |n| "pet #{n}" }
    sequence(:age) { |n| n }
    owner
  end
end
