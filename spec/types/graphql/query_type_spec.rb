require 'rails_helper'

RSpec.describe Types::QueryType do
  describe 'pets' do

    it 'Returns two items' do
      FactoryBot.create_pair(:pet)

      query = %(
        {
          pets {
            name
          }
        }
      )

      result = PetclinicGraphqlRailsSchema.execute(query).as_json

      expect(result.dig('data', 'pets').length).to be(2)
      expect(result.dig('data', 'pets')).to match_array(
        Pet.all.map { |pet| { 'name' => pet.name } }
      )
    end
  end
end
