# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
john = Owner.create({
        name: 'John',
        last_name: 'Doe',
        email: 'john@doe.com'
    })

alice = Owner.create({
        name: 'Alice',
        last_name: 'Baker',
        email: 'alice@baker.com'
    })

Pet.create!([
    {name: 'Rex', age: 17, owner: john},
    {name: 'Lily', age: 4, owner: john},
    {name: 'Avenger', age: 8, owner: alice},
    {name: 'Larry', age: 9, owner: alice},
    {name: 'Gary', age: 11, owner: alice},
])